package dimon.java.HomeWork2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        String word = "tree";
        Scanner in = new Scanner(System.in);
        System.out.println("Enter strings count:");
        int n = Integer.parseInt(in.nextLine());
        String[] strings = new String[n];

        for(int i=0; i<n; i++){
            System.out.println("Enter new string:");
            strings[i]=in.nextLine();//В этой ситуации более оптимальное решение использовать nextLineк
        }

        boolean isContain = false;
        for (String item : strings) {
            item.replace(" ","");
            isContain = item.contains(word);
            if(isContain){
                System.out.println(item);
            }
        }

    }
}
